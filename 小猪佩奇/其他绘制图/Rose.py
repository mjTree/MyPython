﻿import turtle
from turtle import *

#绘制花朵
def flower():
    #fillcolor('red')
    #begin_fill()
    turtle.pencolor('red')
    turtle.left(30)
    turtle.circle(80,80)
    turtle.left(150)
    turtle.circle(-80,30)
    turtle.circle(80,45)
    
    turtle.right(95)
    turtle.circle(-70,110)
    turtle.right(145)
    turtle.circle(75,40)
    turtle.circle(-90,45)

    turtle.penup()
    turtle.goto(28,72)
    turtle.pendown()
    turtle.right(190)
    turtle.circle(110,35)
    turtle.left(30)
    turtle.circle(60,44)
    turtle.left(38)
    turtle.circle(120,35)
    
    
    turtle.penup()
    turtle.goto(9,133)
    turtle.pendown()
    turtle.right(60)
    turtle.circle(-60,46)
    turtle.right(150)
    turtle.circle(-110,18)
    turtle.penup()
    turtle.goto(0,0)
    turtle.pendown()
    #end_fill()


#绘制花枝绿叶
def spray_leaf():
    turtle.right(90)
    turtle.pencolor('cyan')
    turtle.fd(120)
    turtle.backward(50)

    turtle.pencolor('green')
    turtle.right(90)
    turtle.circle(-70,80)
    turtle.right(100)
    turtle.circle(-70,80)
    turtle.right(140)
    turtle.fd(90)
    turtle.backward(90)
    turtle.left(140)
    
    turtle.left(80)
    turtle.circle(70,80)
    turtle.left(100)
    turtle.circle(70,80)
    turtle.left(140)
    turtle.fd(90)

def like():
    turtle.penup()
    turtle.goto(0,200)
    turtle.pencolor('blue')
    turtle.pendown()
    turtle.write('宣你-河智苑',align='center',font=('方正舒体',50,'bold'))
    #turtle.tracer(True)
    turtle.hideturtle()

def main():
    turtle.setup(400,600,250,40)
    flower()
    spray_leaf()
    like()
    mainloop()

main()
