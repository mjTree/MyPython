import turtle
from turtle import *

def Love():
    fillcolor("pink")
    begin_fill()
    turtle.left(120)
    turtle.circle(40,200)
    turtle.fd(80)
    turtle.left(90)
    turtle.fd(80)
    turtle.circle(40,200)
    end_fill()

def Break():
    turtle.right(90)
    turtle.color("pink")
    turtle.fd(40)
    turtle.color("black")
    turtle.left(180)
    turtle.fd(30)
    turtle.color("pink")
    turtle.fd(40)
    turtle.color("black")
    turtle.fd(110)
       
def main():
    turtle.setup(800,600,250,40)
    Love()
    Break()
main()

