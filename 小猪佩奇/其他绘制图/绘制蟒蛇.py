import turtle   #导入类库

def drawSnake(rad,angle,len,neckrad):   #圆形轨迹半径位置,圆形的弧度值,长度,
    for i in range(len):
        turtle.circle(rad,angle)
        turtle.circle(-rad,angle)   #这个for循环一个转10次,5上5下
    turtle.circle(rad,angle/2)  #最后绘制头
    turtle.fd(rad)              #画笔先前直线绘制
    turtle.circle(neckrad+1,180)#画笔反方向
    turtle.fd(rad*2/3)

def main():
    turtle.setup(1300,800,0,0)  #创建窗体;窗体的x,y坐标点的x,y
    pythonsize = 30             #初始化画笔的直径
    turtle.pensize(pythonsize)
    turtle.pencolor("green")    #画笔绘制路径颜色
    turtle.seth(-40)            #画笔绘制总体方向
    drawSnake(40,80,5,pythonsize/2) #调用drowSnake方法

main()
