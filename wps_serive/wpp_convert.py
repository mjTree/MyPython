# coding:utf-8

import os

import shutil

from pywpsrpc.rpcwppapi import createWppRpcInstance, wppapi
from pywpsrpc.common import S_OK


def ppt2pdf(request_property):
    result, rpc_ppt = createWppRpcInstance()

    if result != S_OK:
        raise Exception('create the rpc instance is failed', result)
    result, app_wpp = rpc_ppt.getWppApplication()
    try:
        # app_wpp.Visible = False
        presentations = app_wpp.Presentations
        file_path = request_property['input_path']
        result, presentation = presentations.Open(file_path)
        base_path, file_ext = file_path.rsplit('.')
        new_file = f'{base_path}_tmp.pdf'
        if os.path.exists(new_file):
            os.remove(new_file)
        presentation.SaveAs(new_file, wppapi.ppSaveAsPDF)
        presentation.Close()
        # shutil.move(new_file, file_path)
    finally:
        app_wpp.Quit()


aaa = {
    'input_path': '/home/mjtree/文档/1.pptx'

}
ppt2pdf(aaa)


