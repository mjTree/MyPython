# -*- coding: utf-8 -*-

import uuid

BASE_CHARS = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'


class ShortUUID:

    def __init__(self):
        pass

    def _gen_short_id(self):
        u_id = str(uuid.uuid4()).replace("-", '')
        buffer = []
        for idx in range(0, 8):
            start = idx * 4
            end = idx * 4 + 4
            val = int(u_id[start:end], 16)
            buffer.append(BASE_CHARS[val % 0x3E])
        return "".join(buffer)


if __name__ == '__main__':
    count = 0
    index = []
    id_set = set()
    short_uuid = ShortUUID()
    for i in range(0, 1000000):
        u_id = short_uuid._gen_short_id()
        if u_id in id_set:
            count += 1
            index.append(str(i+1))
        else:
            id_set.add(u_id)
    print(f"重复数：{count}，重复率：{count / 10000000}")

