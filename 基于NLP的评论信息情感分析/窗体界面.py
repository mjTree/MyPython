# coding:utf-8

from tkinter import *
import myWord2vec as WV
from tkinter import messagebox
import tkinter.filedialog as filedialog


def showResult():
    result = WV.svm_predict(entry.get())
    if int(result) == 0:
        messagebox.askokcancel('消息框', '测试结果为差评')
    else:
        messagebox.askokcancel('消息框', '测试结果为好评')


def selectFile():
    entry1.delete(0,END)    # 清空entry里面的内容
    #调用filedialog模块的askdirectory()函数去打开文件夹
    filepath = filedialog.askopenfilename() 
    if filepath:
        entry1.insert(0,filepath)    # 将选择好的路径加入到entry里面


def selectStorePath():
    entry2.delete(0,END)
    filepath1 = filedialog.askdirectory() 
    if filepath1:
        filepath1 = filepath1 + '/'
        entry2.insert(0,filepath1)


def batchFile():
    print(entry1.get())
    print(entry2.get())
    pass


root = Tk()
root.title("评价分类系统")
root.geometry("400x300")


Label1 = Label(root,text='情感分析:').grid(row=0,column=0)
entry = Entry(root,textvariable=StringVar(),width=40)       # Entry是Tkinter用来接收字符串输入的控件
entry.grid(row=0,column=1,padx=10,pady=5)
Button(root,text='查看结果',width=10,command=showResult).grid(row=2,column=0,sticky=S,padx=10,pady=5)
Button(root,text='退出',width=10,command=root.quit).grid(row=2,column=1,sticky=E,padx=10,pady=5)
#N、E、S、W、NW、NE、SW、SE、CENTER


Label2 = Label(root,text='文件路径:').grid(row=3,column=0)
entry1 = Entry(root,textvariable=StringVar(),width=40)
entry1.grid(sticky=W,row=3, column=1, columnspan=4, padx=5, pady=5)
Button(root,text='选择文件',width=10,command=selectFile).grid(row=4,column=0,sticky=S,padx=10,pady=5)


Label3 = Label(root,text='存储路径:').grid(row=5,column=0)
entry2 = Entry(root,textvariable=StringVar(),width=40)
entry2.grid(sticky=W, row=5, column=1, columnspan=4, padx=5, pady=5)
Button(root,text='选择文件夹',width=10,command=selectStorePath).grid(row=6,column=0,sticky=E,padx=10,pady=5)
Button(root,text='批处理',width=10,command=batchFile).grid(row=6,column=1,sticky=E,padx=10,pady=5)

mainloop()


