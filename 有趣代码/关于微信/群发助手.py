#coding:utf8

# 节日群发助手
import itchat, time

itchat.auto_login(True)
SINCERE_WISH = u'祝 %s 新年快乐！'

friendList = itchat.get_friends(update=True)[1:]
for friend in friendList:
    try:
        # 群发消息
        itchat.send(SINCERE_WISH % (friend['RemarkName'] or friend['NickName']))
        # 打印做测试
        print((SINCERE_WISH % (friend['RemarkName'] or friend['NickName'])))
    except:
        # 备注或者昵称带有表情符号无法识别出错
        pass
    time.sleep(.5)