#coding:gb2312


# 训练集文件路径
TRAINING_PATH = r'../data/base_vector.fea'

# 测试集文件路径
TESTING_PATH = r'../data/verify_vector.fea'

# 训练集数目
TRAINING_SIZE = 1000000    # 表示100w行数据

# 待测集数目
TESTING_SIZE = 100    # 表示100行数据

# 数据维度
DATA_DIMENSION = 1025    # 1025维度

# 建立球树尺寸
BALL_TREE_SZIE = 10000    # 综合考量选择10000

# 是否保存训练的模型
SAVE_MODEL = False    # 默认不保存

# 模型仓库保存路径
MODEL_WAREHOUSE = r'../modleWarehouse/'
