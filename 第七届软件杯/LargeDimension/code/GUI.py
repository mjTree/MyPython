#!/usr/bin/env python
#!encoding:utf-8
#!filename:test_filedialog.py


from tkinter import *
import tkinter.filedialog as filedialog

 
def callback():
    entry.delete(0,END)    # 清空entry里面的内容
    #调用filedialog模块的askdirectory()函数去打开文件夹
    global filepath
    #filepath = filedialog.askdirectory()
    filepath = filedialog.askopenfilename()
    if filepath:
        entry.insert(0,filepath)    # 将选择好的路径加入到entry里面
    print(filepath)

def callback1():
    entry1.delete(0,END)
    global filepath1
    filepath1 = filedialog.askdirectory() 
    if filepath1:
        filepath1 = filepath1 + '/'
        entry1.insert(0,filepath1)
    print(filepath1)


if __name__ == "__main__":
    root = Tk()
    root.title("标题狗")
    root.geometry("400x400")
    
    entry = Entry(root, width=60)
    entry.grid(sticky=W+N, row=0, column=0, columnspan=4, padx=5, pady=5)
    button = Button(root,text="选择训练集文件",command=callback)
    button.grid(sticky=W+N, row=1, column=0, padx=5, pady=5)
    
    entry1 = Entry(root, width=60)
    entry1.grid(sticky=W+N, row=2, column=0, columnspan=4, padx=5, pady=5)
    button1 = Button(root,text="选择训练集文件夹",command=callback1)
    button1.grid(sticky=W+N, row=3, column=0, padx=5, pady=5)
    root.mainloop()




