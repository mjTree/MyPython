#coding:gb2312

import time
import struct
import numpy as np
from sklearn.neighbors import NearestNeighbors

TRAIN = []


"""
function: 获取当前毫秒级时间
"""
def get_time_stamp():
    ct = time.time()
    local_time = time.localtime(ct)
    data_head = time.strftime("%Y-%m-%d %H:%M:%S", local_time)
    data_secs = (ct - long(ct)) * 1000
    return "%s.%03d" % (data_head, data_secs)


"""
function: 完整读取二进制文件
parameter: filename 文件路径
parameter: nx 行数
parameter: nz 列数
return: dataList 返回文件存储的信息
"""
def xshow(filename, nx, nz):
    f = open(filename, "rb")
    dataList = []
    for i in range(nx):
        tmp = []
        for j in range(nz):
            data = f.read(4)
            elem = struct.unpack("f", data)[0]
            if j != 0:
                tmp.append(elem)
        dataList.append(tmp)
    f.close()
    return dataList


"""
function: 数据部分读取处理,调用外方法实现最近领查找
parameter: filename 文件路径
parameter: nx 
"""
def loadPartData(filename, nx ,nz):
    Count = 1
    f = open(filename, "rb")
    for i in range(nx):
        tmp = []
        for j in range(nz):
            data = f.read(4)
            elem = struct.unpack("f", data)[0]
            if j != 0:
                tmp.append(elem)
        TRAIN.append(tmp)
        # 样本较大,每读取1w次建树查找
        if (i+1)%10000 == 0:
            print '建树时间'
            global TRAIN    # 修改全局变量TRAIN值需要global声明
            if Count%2 != 0:
                distances, indices, TRAIN = ballTree(TRAIN,TEST)
                indices = IDRestore(indices, Count)
            else:
                distances1, indices1, TRAIN = ballTree(TRAIN,TEST)
                indices1 = IDRestore(indices1, Count)
                distances1, indices1 = sizeCompare(distances,indices,distances1,indices1,Count)
            Count = Count + 1
    f.close()
    return distances1, indices1


"""
function: 根据相应训练集建立球树,查找测试集的最近值和ID
parameter: training 训练集
return: distances 测试集的最近距离值
return: indices 对应训练集的位置
"""
def ballTree(training,testing):
    print get_time_stamp()
    clf = NearestNeighbors(n_neighbors=2)
    clf.fit(training)
    distances, indices = clf.kneighbors(testing, 1)
    '''
    print get_time_stamp()
    nbrs = NearestNeighbors(n_neighbors=2, algorithm="ball_tree", metric='euclidean').fit(training)
    distances, indices = nbrs.kneighbors(training)
    print get_time_stamp()
    training = training[0:1]
    '''
    return distances, indices, training


"""
function: 因为分割读取导致建树时ID改变,修改原ID
parameter: indices 现在的坐标值
parameter: count 循环次数
"""
def IDRestore(indices, count):
    for i in range(len(indices)):
        indices[i][1] = indices[i][1] + 10000*(count-1) - 1
    return indices


"""
function: 比较值大小
parameter:
parameter:
"""
def sizeCompare(distances, indices, distances1, indices1, count):
    #distances = minimun(distances, distances1)
    #indices = minimun(indices, indices1)
    for i in range(len(indices)):
        for j in range(len(indices[0])):
            if distances[i][j] > distances1[i][j]:
                distances[i][j] = distances1[i][j]
                indices[i][j] = indices1[i][j]
    return distances,indices




filename = 'D:\\zzz\\ML\\LargeDimension\\data\\verify_vector.fea'   #100,1025
filename1 = 'D:\\zzz\\ML\\LargeDimension\\data\\base_vector.fea'   #100000000,1025
#test = xshow(filename,10,1025)

"""
print '开始时间: '+get_time_stamp()
test = xshow(filename,100,1025)
#TRAIN.extend(test)
#TRAIN.append(test[14])
TRAIN.extend(xshow(filename1,10000,1025))
#distances, indices = loadPartData(filename1, 40000, 1025) 
print '开始建树: '+get_time_stamp()
'''
nbrs = NearestNeighbors(n_neighbors=2, algorithm='ball_tree').fit(TRAIN)
print '开始查询: '+get_time_stamp()
distances, indices = nbrs.kneighbors(TRAIN)
'''
clf = NearestNeighbors(n_neighbors=2)
clf.fit(TRAIN)
a = [];a.append(test[13])
print '开始查询: '+get_time_stamp()
distances, indices = clf.kneighbors(a, 2)
print(indices);print(distances)
print '结束时间: '+get_time_stamp()
"""




'''
X = np.array([[-1, -1], [-2, -1], [-3, -2], [1, 1], [2, 1], [3, 2]])
clf = NearestNeighbors(n_neighbors=2)
print get_time_stamp()
clf.fit(X)
test = [[-2,-1]]
distance, indice = clf.kneighbors([test[0]], 1)
print distance
print indice
print get_time_stamp()
'''


